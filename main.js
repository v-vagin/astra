const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const fs = require('fs');
const CMD = require('./src/electron/command-enum');
const TEXT = require('./src/electron/text-const');
const PLATFROM = require('./src/electron/platfrom-enum');
const path = require("path");
let preload = path.join(__dirname, 'src', 'electron', 'preload.js'); // full path to preload.js file
let workDir = `${__dirname}`; // get copy of current directory, not reference
//const url = require("url");

/**
 * Uncomment next line for electron hot reload during development
 */
//require('electron-reload')(path.join(__dirname, 'astra-angular'));


/**
 * File type:
 * 0 - Unknown type
 * 1 - File
 * 2 - Directory
 * 3 - Block device
 * 4 - Character device
 * 5 - Symbolic link
 * 6 - (FIFO) pipe
 * 7 - Socket
 * 8 - Socket
 */
const fileType = { NONE: 0, FILE: 1, DIR: 2, BDEV: 3, CDEV: 4, LINK: 5, PIPE: 6, SOCKET: 7, PARENT: 8 };

let mainWindow;

/**
 * One of platform: aix, darwin, freebsd, linux, openbsd, sunos, win32
 */
let platform = PLATFROM.NONE;
switch (process.platform) {
  case 'aix': platform = PLATFROM.AIX; break;
  case 'darwin': platform = PLATFROM.DARWIN; break;
  case 'freebsd': platform = PLATFROM.FREEBSD; break;
  case 'linux': platform = PLATFROM.LINUX; break;
  case 'openbsd': platform = PLATFROM.OPENBSD; break;
  case 'sunos': platform = PLATFROM.SUNOS; break;
  case 'win32': platform = PLATFROM.WIN32; break;
  case 'posix': platform = PLATFROM.POSIX; break;
}

//#region Functions
function runApp () {
  mainWindow = new BrowserWindow({
    width: 1600,
    height: 800,
    webPreferences: {
      preload: preload,
      // Context isolation and Node integration
      // Scenario contextIsolation  nodeIntegration Remarks
      // A        false             false           Preload is not needed. Node.js is available in the Main but not in the Renderer.
      // B        false             true            Preload is not needed. Node.js is available in the Main and Renderer.
      // C        true              false           Preload is needed. Node.js is available in the Main and Preload but not in the Renderer. Default. Recommended.
      // D        true              true            Preload is needed. Node.js is available in the Main, Preload, and Renderer.
      contextIsolation: true,
      nodeIntegration: true,
      //enableRemoteModule: true
    }
  });

  // mainWindow.loadURL(
  //   url.format({
  //     pathname: path.join(__dirname, `/astra-angular/index.html`),
  //     protocol: "file:",
  //     slashes: true
  //   })
  // );

  let menu = getMainMenu();
  Menu.setApplicationMenu(menu);
  mainWindow.loadFile('./astra-angular/index.html');
  // mainWindow.webContents.openDevTools(); // Open the DevTools.
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

function quitApp() {
  app.quit();
}

/**
 * Prepare main menu of Electron application
 * @returns
 */
function getMainMenu() {
  return Menu.buildFromTemplate([
    {
      label: TEXT.FILE,
      submenu: [
        {
          label: TEXT.NEW_FILE,
          click: () => {
            mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_NEW_FILE, data: workDir });
          }
        },
        {
          label: TEXT.OPEN_FILE,
          click: () => {
            mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_OPEN_FILE });
          }
        },
        {
          label: TEXT.SAVE_FILE,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_SAVE_FILE, data: null })
        },
        {
          label: TEXT.SAVE_FILE_AS,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_SAVE_FILE_AS, data: null })
        },
        {
          label: TEXT.CLOSE_FILE,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_CLOSE_FILE, data: null })
        },
        {
          type: 'separator'
        },
        {
          label: TEXT.EXIT,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_EXIT, data: null })
        }
      ]
    },
    {
      label: TEXT.HELP,
      submenu: [
        {
          label: TEXT.CONTENTS,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_HELP, data: null })
        },
        {
          type: 'separator'
        },
        {
          label: TEXT.ABOUT,
          click: () => mainWindow.webContents.send(TEXT.FROM_MAIN, { cmd: CMD.MENU_ABOUT, data: null })
        }
      ]
    }
  ]);
}

/**
 * Convert bool to number
 * @param {*} value bool value
 * @returns 1 - if input value is true, 0  if any other value: false, null or undefined
 */
function bton(value) {
  return value ? 1 : 0;
}

/**
 * Get file type
 * @param {*} value value of {@link fs.StatsBase}
 * @returns @see fileType
 */
function getFileType(value) {
  return !value
    ? fileType.NONE
    : bton(value.isFile()) * fileType.FILE ||
      bton(value.isDirectory()) * fileType.DIR ||
      bton(value.isBlockDevice()) * fileType.BDEV ||
      bton(value.isCharacterDevice()) * fileType.CDEV ||
      bton(value.isSymbolicLink()) * fileType.LINK ||
      bton(value.isFIFO()) * fileType.PIPE ||
      bton(value.isSocket()) * fileType.SOCKET;
}

/**
 * Get content of directory
 * @param {*} dir path to directory for getting content
 * @returns Sorted array of files in directory. At the beginning are sorted catalogs and then sorted files. Each array's items in next format:
 * file name
 * file type {@link fileType}
 * file size
 * atime - indicates the last date and time a file was read by a user, in milliseconds
 * ctime - indicates the last date and time some metadata of a file was changed, for example, if permission settings of a file were modified, in milliseconds
 * mtime - indicates the last date and time the contents of a file were modified, in milliseconds
 * btime - indicates the date and time of creating file, in milliseconds
 */
function getDirContent(dir) {
  let result = [];
  let dlist = [];
  let flist = [];
  let isOk = true;
  let errMsg = '';
  try {
    let files = fs.readdirSync(dir, { encoding: "utf8", withFileTypes: false, recursive: false }) || [];
    for (let f of files) {
      let fp = path.join(dir, f);
      // skip directory entry with no permission
      try {
        let s = fs.statSync(fp, { throwIfNoEntry: false, bigint: false });
        let ft = getFileType(s);

        if (ft == fileType.DIR) {
          dlist.push({ name: f, type: ft, size: s.size, atime: s.atimeMs, ctime: s.ctimeMs, mtime: s.mtimeMs, btime: s.birthtimeMs });
        }
        else {
          flist.push({ name: f, type: ft, size: s.size, atime: s.atimeMs, ctime: s.ctimeMs, mtime: s.mtimeMs, btime: s.birthtimeMs });
        }
      } catch (error) {
        continue;
      }
    }
    dlist.sort();
    flist.sort();
  } catch (error) {
    errMsg = error;
    isOk = false;
  }
  result = { ok: isOk, error: errMsg, data: [{ name: dir, type: fileType.PARENT, size: 0, atime: 0, ctime: 0, mtime: 0, btime: 0 }, ...dlist, ...flist] };
  return result;
}
//#endregion

// Command handler for requests from renderer
app.whenReady().then(() => {
  ipcMain.handle(TEXT.TO_MAIN, (e, request) => { // async (e, request) => { if using await operation
    console.log(`Command '${request.cmd}' processing started`);
    //await operation
    try {
      switch (request.cmd) {
        case CMD.GET_DIR_CONTENT: {
          let path = !request.data ? workDir : request.data;
          let result = getDirContent(path);

          if (!result || !result.ok) {
            return { ok: false, error: result.error };
          }

          return { ok: true, data: result.data };
        }

        case CMD.GET_ENV: {
          try {
            let env = JSON.parse(JSON.stringify(process.env));
            return { ok: true, data: { platform: platform, cwd: workDir, env: env } };
          } catch (error) {
            return { ok: false, error: error };
          }
        }

        case CMD.GET_WORK_DIR: {
          return { ok: true, data: workDir };
        }

        case CMD.READ_FILE: {
          try {
            let fileContent = fs.readFileSync(request.data, 'utf8');
            return { ok: true, data: fileContent };
          } catch (error) {
            console.log('###', error)
            return { ok: false, error: error };
          }
        }

        case CMD.QUIT_APP: {
          quitApp();
          return { ok: true };
        }

      }
      return { ok: false, error: 'Unknown command' };
    } catch (error) {
      return { ok: false, error: error };
    }
  });

  runApp();

  // Необходимо для macOS
  app.on('activate', function () {
    if (BrowserWindow.getAllWindows.length === 0) runApp();
  });

  // app.on('app:exit', function () {
  //   app.quit();
  // });

});

app.on('window-all-closed', function () {
  if (platform !== 'darwin') app.quit()
});

console.log("Main process started on:", process.platform);
