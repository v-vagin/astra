const NONE = 0;
const AIX = 1;
const DARWIN = 2;
const FREEBSD = 3;
const LINUX = 4;
const OPENBSD = 5;
const SUNOS = 6;
const WIN32 = 7;
const POSIX = 8;

exports.NONE = NONE;
exports.AIX = AIX;
exports.DARWIN = DARWIN;
exports.FREEBSD = FREEBSD;
exports.LINUX = LINUX;
exports.OPENBSD = OPENBSD;
exports.SUNOS = SUNOS;
exports.WIN32 = WIN32;
exports.POSIX = POSIX;
