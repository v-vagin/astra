// Main Menu
const FILE = 'File';
const HELP = 'Help';
const NEW_FILE = 'New file';
const OPEN_FILE = 'Open file';
const SAVE_FILE = 'Save file';
const SAVE_FILE_AS = 'Save file as';
const CLOSE_FILE = 'Close file';
const NEW_PROJ = 'New project';
const OPEN_PROJ = 'Open project';
const SAVE_PROJ = 'Save project';
const SAVE_PROJ_AS = 'Save project as';
const CLOSE_PROJ = 'Close project';
const EXIT = 'Exit';
const CONTENTS = 'Contents';
const ABOUT = 'About';

exports.FILE = FILE;
exports.HELP = HELP;
exports.NEW_FILE = NEW_FILE;
exports.OPEN_FILE = OPEN_FILE;
exports.SAVE_FILE = SAVE_FILE;
exports.SAVE_FILE_AS = SAVE_FILE_AS;
exports.CLOSE_FILE = CLOSE_FILE;
exports.NEW_PROJ = NEW_PROJ;
exports.OPEN_PROJ = OPEN_PROJ;
exports.SAVE_PROJ = SAVE_PROJ;
exports.SAVE_PROJ_AS = SAVE_PROJ_AS;
exports.CLOSE_PROJ = CLOSE_PROJ;
exports.EXIT = EXIT;
exports.CONTENTS = CONTENTS;
exports.ABOUT = ABOUT;

// Channels
const FROM_MAIN = 'main-to-renderer';
const TO_MAIN = 'renderer-to-main';

exports.FROM_MAIN = FROM_MAIN;
exports.TO_MAIN = TO_MAIN;
