const { contextBridge, ipcRenderer } = require('electron');
const TEXT = require('./text-const');

contextBridge.exposeInMainWorld('electron', {
  fromMain: (callback) => ipcRenderer.on(TEXT.FROM_MAIN, callback),
  toMain: (data) => ipcRenderer.invoke(TEXT.TO_MAIN, data),
  version: {
    node: () => process.versions.node,
    chrome: () => process.versions.chrome,
    electron: () => process.versions.electron
  },
  editorLanguage: null
});

console.log("Preload process started");
