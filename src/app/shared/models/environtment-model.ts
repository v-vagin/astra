import { PlatformTypeEnum } from "../enums/platform-type-enum";

export class EnvironmentModel {
  platform: PlatformTypeEnum = PlatformTypeEnum.NONE;
  cwd: string = '';
  env: any;
}
