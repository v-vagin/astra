import { FileTypeEnum } from "../enums/file-type-enum"

export class DirectoryEntryModel {
  /**
   * file/directory name
   */
  name: string = '';
  /**
   * file/directory type {@link FileTypeEnum}
   */
  type: FileTypeEnum = FileTypeEnum.NONE;
  /**
   * file size, in bytes
   */
  size: number = 0;
  /**
   * indicates the last date and time a file was read by a user, in milliseconds
   */
  atime: number = 0;
  /**
   * indicates the last date and time some metadata of a file was changed, for example, if permission settings of a file were modified, in milliseconds
   */
  ctime: number = 0;
  /**
   * indicates the last date and time the contents of a file were modified, in milliseconds
   */
  mtime: number = 0;
  /**
   * indicates the date and time of creating file, in milliseconds
   */
  btime: number = 0;
}
