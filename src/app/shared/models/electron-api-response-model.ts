export class ElectronApiResponsetModel<T> {
  ok!: boolean;
  error?: string;
  data?: T;

  constructor(ok: boolean, error?: string, data?: T) {
    this.ok = ok;
    this.error = error;
    this.data = data;
  }
}