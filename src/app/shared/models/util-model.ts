import { FileTypeEnum } from "../enums/file-type-enum";
import { IStatsBase } from "./stats-base-model";

export class Util {
  static isArray(arg: any) { return Array.isArray(arg); }
  static isBoolean(arg: any) { return typeof arg === 'boolean'; }
  static isNull(arg: any) { return arg === null; }
  static isNullOrUndefined(arg: any) { return arg == null; }
  static isNumber(arg: any) { return typeof arg === 'number'; }
  static isString(arg: any) { return typeof arg === 'string'; }
  static isSymbol(arg: any) { return typeof arg === 'symbol'; }
  static isUndefined(arg: any) { return arg === void 0; }
  static isRegExp(arg: any) { return this.isObject(arg) && this.objectToString(arg) === '[object RegExp]'; }
  static isObject(arg: any) { return typeof arg === 'object' && arg !== null; }
  static isDate(arg: any) { return this.isObject(arg) && this.objectToString(arg) === '[object Date]'; }
  static isError(arg: any) { return this.isObject(arg) && (this.objectToString(arg) === '[object Error]' || arg instanceof Error); }
  static isFunction(arg: any) { return typeof arg === 'function'; }
  static isPrimitive(arg: any) {
    return arg === null ||
      typeof arg === 'boolean' ||
      typeof arg === 'number' ||
      typeof arg === 'string' ||
      typeof arg === 'symbol' ||  // ES6 symbol
      typeof arg === 'undefined';
  }
  // static isBuffer(arg: any) { return arg instanceof Buffer; }
  // static isBufferBrowser(arg: any) {
  //   return arg && typeof arg === 'object'
  //     && typeof arg.copy === 'function'
  //     && typeof arg.fill === 'function'
  //     && typeof arg.readUInt8 === 'function';
  // }
  static objectToString(arg: any) { return Object.prototype.toString.call(arg); }
  static pad(arg: any) { return arg < 10 ? '0' + arg.toString(10) : arg.toString(10); }
  /**
  * Formats a file size into a human-readable string
  * @param {*} fileSize value of file size
  * @returns Formated file size into a human-readable string
  */
  static formatFileSize(fileSize: number) {
    if (fileSize < 1000000) {
      return `${fileSize}`;
    }

    if (fileSize < 10000000) {
      return `${Math.round(fileSize / 1024)} K`;
    }

    return `${Math.round(fileSize / 1048576)} M`;
  }
  /**
   * Convert milliseconds to timestamp string
   * @param {*} ms timestamp in milliseconds
   * @returns timestamp string in format d-MM-YYYY HH:MM:SS
   */
  static mstodate(ms: number): string {
    let d = new Date(ms || 0);
    let dd = d.getDate();
    let dm = d.getMonth() + 1;
    let dms = dm.toString().padStart(2, '0');
    let dy = d.getFullYear();
    let th = d.getHours();
    let ths = th.toString().padStart(2, '0');
    let tm = d.getMinutes();
    let tms = tm.toString().padStart(2, '0');
    let ts = d.getSeconds();
    let tss = ts.toString().padStart(2, '0');
    return `${dd}-${dms}-${dy} ${ths}:${tms}:${tss}`;
  }
  /**
   * Convert bool to number
   * @param {*} value bool value
   * @returns 1 - if input value is true, 0  if any other value: false, null or undefined
   */
  static bton(value: boolean): number {
    return value ? 1 : 0;
  }
  /**
   * Get file type
   * @param {*} value value of {@link StatsBase}
   * @returns @see fileType
   */
  static getFileType(value: IStatsBase<number>) {
    return !value
      ? FileTypeEnum.NONE
      : this.bton(value.isFile()) * FileTypeEnum.FILE ||
        this.bton(value.isDirectory()) * FileTypeEnum.DIR ||
        this.bton(value.isBlockDevice()) * FileTypeEnum.BDEV ||
        this.bton(value.isCharacterDevice()) * FileTypeEnum.CDEV ||
        this.bton(value.isSymbolicLink()) * FileTypeEnum.LINK ||
        this.bton(value.isFIFO()) * FileTypeEnum.PIPE ||
        this.bton(value.isSocket()) * FileTypeEnum.SOCKET;
  }

}