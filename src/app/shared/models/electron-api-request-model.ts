import { CommandEnum } from "../enums/command-enum";

export class ElectronApiRequestModel<T> {
  public cmd: CommandEnum;
  public data?: T;

  constructor(command: CommandEnum, data?: T) {
    this.cmd = command;
    this.data = data;
  }
}
