import { PlatformTypeEnum } from "../enums/platform-type-enum";
import { Util } from "./util-model";

class PathObject {
  root?: string;
  dir?: string;
  base?: string;
  ext?: string;
  name?: string;
}

class StatPath {
  device?: string;
  isUnc!: boolean;
  isAbsolute!: boolean;
  tail?: string;
}

export class Path {
  private osType: PlatformTypeEnum;
  // Regex to split a windows path into three parts: [*, device, slash, tail] windows-only
  private splitDeviceRe = /^([a-zA-Z]:|[\\\/]{2}[^\\\/]+[\\\/]+[^\\\/]+)?([\\\/])?([\s\S]*?)$/;
  // Regex to split the tail part of the above into [*, dir, basename, ext]
  private splitTailRe = /^([\s\S]*?)((?:\.{1,2}|[^\\\/]+?|)(\.[^.\/\\]*|))(?:[\\\/]*)$/;
  // Split a filename into [root, dir, basename, ext], unix version
  // 'root' is just a slash, or nothing.
  private splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;

  private cwd!: string;
  private env!: any;

  readonly sep?: string;
  readonly delimiter?: string;

  basename(path: string, ext?: string): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Basename(path, ext) : this.posixBasename(path, ext); }
  dirname(path: string): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Dirname(path) : this.posixDirname(path); }
  extname(path: string): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Extname(path) : this.posixExtname(path); }
  format(pathObject: PathObject): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Format(pathObject) : this.posixFormat(pathObject); }
  isAbsolute(path: string): boolean { return this.osType == PlatformTypeEnum.WIN32 ? this.win32IsAbsolute(path) : this.posixIsAbsolute(path); }
  join(...paths: string[]): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Join(...paths) : this.posixJoin(...paths); }
  normalize(path: string): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Normalize(path) : this.posixNormalize(path); }
  parse(path: string): PathObject { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Parse(path) : this.posixParse(path); }
  relative(from: string, to: string): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Relative(from, to) : this.posixRelative(from, to); }
  resolve(...paths: string[]): string { return this.osType == PlatformTypeEnum.WIN32 ? this.win32Resolve(...paths) : this.posixResolve(...paths); }

  constructor(os?: PlatformTypeEnum, cwd?: string, env?: any) {
    this.cwd = !cwd ? '.' : cwd;
    this.env = !env ? {} : env;
    switch (os) {
      case PlatformTypeEnum.WIN32: {
        this.osType = PlatformTypeEnum.WIN32;
        this.sep = '\\';
        this.delimiter = ';';
        break;
      }
      default: {
        this.osType = PlatformTypeEnum.POSIX;
        this.sep = '/';
        this.delimiter = ':';
        break;
      }
    }
  }

  /**
   * resolves . and .. elements in a path array with directory names there
   * must be no slashes or device names (c:\) in the array
   * (so also no leading and trailing slashes - it does not distinguish
   * relative and absolute paths)
   * @param parts
   * @param allowAboveRoot
   * @returns
   */
  private normalizeArray(parts: any[], allowAboveRoot: boolean): any[] {
    let res = [];
    for (let i = 0; i < parts.length; i++) {
      let p = parts[i];

      // ignore empty parts
      if (!p || p === '.')
        continue;

      if (p === '..') {
        if (res.length && res[res.length - 1] !== '..') {
          res.pop();
        }
        else {
          if (allowAboveRoot) {
            res.push('..');
          }
        }
      }
      else {
        res.push(p);
      }
    }

    return res;
  }

  /**
   * returns an array with empty elements removed from either end of the input
   * array or the original array if no elements need to be removed
   * @param arr
   * @returns
   */
  private trimArray(arr: any[]): any[] {
    let lastIndex = arr.length - 1;
    let start = 0;
    for (; start <= lastIndex; start++) {
      if (arr[start])
        break;
    }

    let end = lastIndex;
    for (; end >= 0; end--) {
      if (arr[end])
        break;
    }

    if (start === 0 && end === lastIndex)
      return arr;

    if (start > end)
      return [];

    return arr.slice(start, end + 1);
  }

  /**
   * Function to split a filename into [root, dir, basename, ext]
   * @param filename
   * @returns
   */
  private win32SplitPath(filename: string): any[] {
    // Separate device+slash from tail
    let result = this.splitDeviceRe.exec(filename) || [];
    let device = (result[1] || '') + (result[2] || '');
    let tail = result[3] || '';
    // Split the tail into dir, basename and extension
    let result2 = this.splitTailRe.exec(tail) || [];
    let dir = result2[1];
    let basename = result2[2];
    let ext = result2[3];
    return [device, dir, basename, ext];
  }

  private win32StatPath(path: string): StatPath {
    let result = this.splitDeviceRe.exec(path) || [];
    let device = result[1] || '';
    let isUnc = !!device && device[1] !== ':';
    return {
      device: device,
      isUnc: isUnc,
      isAbsolute: isUnc || !!result[2], // UNC paths are always absolute
      tail: result[3]
    };
  }

  private normalizeUNCRoot(device: string) {
    return '\\\\' + device.replace(/^[\\\/]+/, '').replace(/[\\\/]+/g, '\\');
  }

  /**
   * path.resolve([from ...], to)
   * @returns
   */
  private win32Resolve(...args: string[]): string {
    let resolvedDevice = '';
    let resolvedTail = '';
    let resolvedAbsolute = false;
    let isUnc = false;

    for (let i = args.length - 1; i >= -1; i--) {
      let path;

      if (i >= 0) {
        path = args[i];
      }
      else {
        if (!resolvedDevice) {
          path = this.cwd;
        }
        else {
          // Windows has the concept of drive-specific current working
          // directories. If we've resolved a drive letter but not yet an
          // absolute path, get cwd for that drive. We're sure the device is not
          // an unc path at this points, because unc paths are always absolute.
          path = this.env['=' + resolvedDevice];
          // Verify that a drive-local cwd was found and that it actually points
          // to our drive. If not, default to the drive's root.
          if (!path || path.substr(0, 3).toLowerCase() !== resolvedDevice.toLowerCase() + '\\') {
            path = resolvedDevice + '\\';
          }
        }
      }

      // Skip empty and invalid entries
      if (!Util.isString(path)) {
        throw new TypeError('Arguments to path.resolve must be strings');
      }
      else {
        if (!path) {
          continue;
        }
      }

      let result = this.win32StatPath(path);
      let device = result.device;
      isUnc = result.isUnc;
      let isAbsolute = result.isAbsolute;
      let tail = result.tail;

      if (device &&
          resolvedDevice &&
          device.toLowerCase() !== resolvedDevice.toLowerCase()
      ) {
        // This path points to another device so it is not applicable
        continue;
      }

      if (device && !resolvedDevice) {
        resolvedDevice = device;
      }

      if (!resolvedAbsolute) {
        resolvedTail = tail + '\\' + resolvedTail;
        resolvedAbsolute = isAbsolute;
      }

      if (resolvedDevice && resolvedAbsolute) {
        break;
      }
    }

    // Convert slashes to backslashes when `resolvedDevice` points to an UNC
    // root. Also squash multiple slashes into a single one where appropriate.
    if (isUnc) {
      resolvedDevice = this.normalizeUNCRoot(resolvedDevice);
    }

    // At this point the path should be resolved to a full absolute path,
    // but handle relative paths to be safe (might happen when process.cwd()
    // fails)

    // Normalize the tail path
    resolvedTail = this.normalizeArray(resolvedTail.split(/[\\\/]+/), !resolvedAbsolute).join('\\');
    return (resolvedDevice + (resolvedAbsolute ? '\\' : '') + resolvedTail) || '.';
  };

  private win32Normalize(path: string): string {
    let result = this.win32StatPath(path);
    let device = result.device || '';
    let isUnc = result.isUnc;
    let isAbsolute = result.isAbsolute;
    let tail = result.tail || '';
    let trailingSlash = /[\\\/]$/.test(tail);

    // Normalize the tail path
    tail = this.normalizeArray(tail.split(/[\\\/]+/), !isAbsolute).join('\\');

    if (!tail && !isAbsolute) {
      tail = '.';
    }

    if (tail && trailingSlash) {
      tail += '\\';
    }

    // Convert slashes to backslashes when `device` points to an UNC root.
    // Also squash multiple slashes into a single one where appropriate.
    if (isUnc) {
      device = this.normalizeUNCRoot(device);
    }

    return device + (isAbsolute ? '\\' : '') + tail;
  };

  private win32IsAbsolute(path: string): boolean {
    return this.win32StatPath(path).isAbsolute;
  };

  private win32Join(...args: string[]): string {
    let paths = [];
    for (let i = 0; i < args.length; i++) {
      let arg = args[i];
      if (!Util.isString(arg)) {
        throw new TypeError('Arguments to path.join must be strings');
      }
      if (arg) {
        paths.push(arg);
      }
    }

    let joined = paths.join('\\');

    // Make sure that the joined path doesn't start with two slashes, because
    // normalize() will mistake it for an UNC path then.
    //
    // This step is skipped when it is very clear that the user actually
    // intended to point at an UNC path. This is assumed when the first
    // non-empty string arguments starts with exactly two slashes followed by
    // at least one more non-slash character.
    //
    // Note that for normalize() to treat a path as an UNC path it needs to
    // have at least 2 components, so we don't filter for that here.
    // This means that the user can use join to construct UNC paths from
    // a server name and a share name; for example:
    //   path.join('//server', 'share') -> '\\\\server\\share\')
    if (!/^[\\\/]{2}[^\\\/]/.test(paths[0])) {
      joined = joined.replace(/^[\\\/]{2,}/, '\\');
    }

    return this.win32Normalize(joined);
  };

  /**
   * path.relative(from, to)
   * it will solve the relative path from 'from' to 'to', for instance:
   * from = 'C:\\orandea\\test\\aaa'
   * to = 'C:\\orandea\\impl\\bbb'
   * The output of the function should be: '..\\..\\impl\\bbb'
   * @param from
   * @param to
   * @returns
   */
  private win32Relative(from: string, to: string): string {
    from = this.win32Resolve(from);
    to = this.win32Resolve(to);

    // windows is not case sensitive
    let lowerFrom = from.toLowerCase();
    let lowerTo = to.toLowerCase();

    let toParts = this.trimArray(to.split('\\'));

    let lowerFromParts = this.trimArray(lowerFrom.split('\\'));
    let lowerToParts = this.trimArray(lowerTo.split('\\'));

    let length = Math.min(lowerFromParts.length, lowerToParts.length);
    let samePartsLength = length;
    for (let i = 0; i < length; i++) {
      if (lowerFromParts[i] !== lowerToParts[i]) {
        samePartsLength = i;
        break;
      }
    }

    if (samePartsLength == 0) {
      return to;
    }

    let outputParts = [];
    for (let i = samePartsLength; i < lowerFromParts.length; i++) {
      outputParts.push('..');
    }

    outputParts = outputParts.concat(toParts.slice(samePartsLength));

    return outputParts.join('\\');
  };

  private win32MakeLong(path: string): string {
    // Note: this will *probably* throw somewhere.
    if (!Util.isString(path)) {
      return path;
    }

    if (!path) {
      return '';
    }

    let resolvedPath = this.win32Resolve(path);

    if (/^[a-zA-Z]\:\\/.test(resolvedPath)) {
      // path is local filesystem path, which needs to be converted
      // to long UNC path.
      return '\\\\?\\' + resolvedPath;
    }
    else {
      if (/^\\\\[^?.]/.test(resolvedPath)) {
      // path is network UNC path, which needs to be converted
      // to long UNC path.
      return '\\\\?\\UNC\\' + resolvedPath.substring(2);
      }
    }

    return path;
  };

  private win32Dirname(path: string): string {
    let result = this.win32SplitPath(path);
    let root = result[0];
    let dir = result[1];

    if (!root && !dir) {
      // No dirname whatsoever
      return '.';
    }

    if (dir) {
      // It has a dirname, strip trailing slash
      dir = dir.substr(0, dir.length - 1);
    }

    return root + dir;
  };

  private win32Basename(path: string, ext?: string): string {
    let f = this.win32SplitPath(path)[2];
    // TODO: make this comparison case-insensitive on windows?
    if (ext && f.substr(-1 * ext.length) === ext) {
      f = f.substr(0, f.length - ext.length);
    }
    return f;
  };

  private win32Extname(path: string): string {
    return this.win32SplitPath(path)[3];
  };

  private win32Format(pathObject: PathObject): string {
    if (!Util.isObject(pathObject)) {
      throw new TypeError("Parameter 'pathObject' must be an object, not " + typeof pathObject);
    }

    let root = pathObject.root || '';

    if (!Util.isString(root)) {
      throw new TypeError("'pathObject.root' must be a string or undefined, not " + typeof pathObject.root);
    }

    let dir = pathObject.dir;
    let base = pathObject.base || '';

    if (!dir) {
      return base;
    }

    if (dir[dir.length - 1] === this.sep) {
      return dir + base;
    }

    return dir + this.sep + base;
  };

  private win32Parse(pathString: string): PathObject {
    if (!Util.isString(pathString)) {
      throw new TypeError("Parameter 'pathString' must be a string, not " + typeof pathString);
    }

    let allParts = this.win32SplitPath(pathString);

    if (!allParts || allParts.length !== 4) {
      throw new TypeError("Invalid path '" + pathString + "'");
    }

    return {
      root: allParts[0],
      dir: allParts[0] + allParts[1].slice(0, -1),
      base: allParts[2],
      ext: allParts[3],
      name: allParts[2].slice(0, allParts[2].length - allParts[3].length)
    };
  };

  private posixSplitPath(filename: string): any[] {
    return this.splitPathRe?.exec(filename)?.slice(1) || [];
  }

  /**
   * path.resolve([from ...], to)
   * posix version
   * @returns
   */
  private posixResolve(...args: string[]): string {
    let resolvedPath = '';
    let resolvedAbsolute = false;

    for (let i = args.length - 1; i >= -1 && !resolvedAbsolute; i--) {
      let path = (i >= 0) ? args[i] : this.cwd;

      // Skip empty and invalid entries
      if (!Util.isString(path)) {
        throw new TypeError('Arguments to path.resolve must be strings');
      } else {
        if (!path) {
        continue;
        }
      }

      resolvedPath = path + '/' + resolvedPath;
      resolvedAbsolute = path[0] === '/';
    }

    // At this point the path should be resolved to a full absolute path, but
    // handle relative paths to be safe (might happen when process.cwd() fails)

    // Normalize the path
    resolvedPath = this.normalizeArray(resolvedPath.split('/'), !resolvedAbsolute).join('/');
    return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
  };

  private posixIsAbsolute(path: string): boolean {
    return path.charAt(0) === '/';
  };

  /**
   * path.normalize(path)
   * @param path
   * @returns
   */
  private posixNormalize(path: string): string {
    let isAbsolute = this.posixIsAbsolute(path);
    let trailingSlash = path && path[path.length - 1] === '/';

    // Normalize the path
    path = this.normalizeArray(path.split('/'), !isAbsolute).join('/');

    if (!path && !isAbsolute) {
      path = '.';
    }

    if (path && trailingSlash) {
      path += '/';
    }

    return (isAbsolute ? '/' : '') + path;
  };

  private posixJoin(...args: string[]): string {
    let path = '';
    for (let i = 0; i < args.length; i++) {
      let segment = args[i];

      if (!Util.isString(segment)) {
        throw new TypeError('Arguments to path.join must be strings');
      }

      if (segment) {
        if (!path) {
          path += segment;
        }
        else {
          path += '/' + segment;
        }
      }
    }
    return this.posixNormalize(path);
  };

  /**
   * path.relative(from, to)
   * @param from
   * @param to
   * @returns
   */
  private posixRelative(from: string, to: string): string {
    from = this.posixResolve(from).substr(1);
    to = this.posixResolve(to).substr(1);

    let fromParts = this.trimArray(from.split('/'));
    let toParts = this.trimArray(to.split('/'));

    let length = Math.min(fromParts.length, toParts.length);
    let samePartsLength = length;
    for (let i = 0; i < length; i++) {
      if (fromParts[i] !== toParts[i]) {
        samePartsLength = i;
        break;
      }
    }

    let outputParts = [];
    for (let i = samePartsLength; i < fromParts.length; i++) {
      outputParts.push('..');
    }

    outputParts = outputParts.concat(toParts.slice(samePartsLength));
    return outputParts.join('/');
  };

  private posixMakeLong(path: string): string {
    return path;
  };

  private posixDirname(path: string): string {
    let result = this.posixSplitPath(path) || [];
    let root = result[0];
    let dir = result[1];

    if (!root && !dir) {
      // No dirname whatsoever
      return '.';
    }

    if (dir) {
      // It has a dirname, strip trailing slash
      dir = dir.substr(0, dir.length - 1);
    }

    return root + dir;
  };

  private posixBasename(path: string, ext?: string): string {
    let f = this.posixSplitPath(path)[2];
    // TODO: make this comparison case-insensitive on windows?
    if (ext && f.substr(-1 * ext.length) === ext) {
      f = f.substr(0, f.length - ext.length);
    }
    return f;
  };

  private posixExtname(path: string): string {
    return this.posixSplitPath(path)[3];
  };

  private posixFormat(pathObject: PathObject): string {
    if (!Util.isObject(pathObject)) {
      throw new TypeError("Parameter 'pathObject' must be an object, not " + typeof pathObject);
    }

    let root = pathObject.root || '';

    if (!Util.isString(root)) {
      throw new TypeError("'pathObject.root' must be a string or undefined, not " + typeof pathObject.root);
    }

    let dir = pathObject.dir ? pathObject.dir + this.sep : '';
    let base = pathObject.base || '';
    return dir + base;
  };

  private posixParse(pathString: string): PathObject {
    if (!Util.isString(pathString)) {
      throw new TypeError("Parameter 'pathString' must be a string, not " + typeof pathString);
    }

    let allParts = this.posixSplitPath(pathString);

    if (!allParts || allParts.length !== 4) {
      throw new TypeError("Invalid path '" + pathString + "'");
    }

    allParts[1] = allParts[1] || '';
    allParts[2] = allParts[2] || '';
    allParts[3] = allParts[3] || '';

    return {
      root: allParts[0],
      dir: allParts[0] + allParts[1].slice(0, -1),
      base: allParts[2],
      ext: allParts[3],
      name: allParts[2].slice(0, allParts[2].length - allParts[3].length)
    };
  };

}
