import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { EditorLanguageType } from '../../enums/editor-language-enum';

export class NewFileDialogData {
  name?: string;
  language?: EditorLanguageType
}

interface ILanguage {
  value: EditorLanguageType;
  name: string;
}

@Component({
  selector: 'app-new-file-dialog',
  templateUrl: './new-file-dialog.component.html'
})
export class NewFileDialogComponent {
  public file: NewFileDialogData;
  languages: ILanguage[] = [
    {value: '', name: 'Text'},
    {value: 'typescript', name: 'TypeScript'},
    {value: 'javascript', name: 'JavaScript'},
    {value: 'html', name: 'HTML'},
    {value: 'scss', name: 'SCSS'},
    {value: 'css', name: 'CSS'},
    {value: 'json', name: 'JSON'},
  ];
  constructor(
    public dialogRef: MatDialogRef<NewFileDialogComponent>
  ) {
    console.log('OpenFileDialogComponent[CTOR]');
    this.file = {name: 'newfile', language: 'typescript'};
  }

  closeDialog(data: NewFileDialogData) {
    this.dialogRef.close(data);
  }
}
