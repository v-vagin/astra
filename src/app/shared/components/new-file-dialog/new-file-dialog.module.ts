import { NgModule } from '@angular/core';
import { AngularModule } from 'src/app/shared/modules/angular.module';

import { NewFileDialogComponent } from './new-file-dialog.component';

@NgModule({
  declarations: [
    NewFileDialogComponent
  ],
  imports: [
    AngularModule,
  ],
  exports: [
    NewFileDialogComponent
  ]
})
export class NewFileDialogModule { }
