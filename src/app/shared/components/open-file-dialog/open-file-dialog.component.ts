import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { RendererService } from '../../services/renderer-service';

import { DirectoryEntryModel } from '../../models/directory-entry-model';
import { FileTypeEnum } from '../../enums/file-type-enum';
import { Util } from "src/app/shared/models/util-model";
import { Path } from '../../models/path-model';

export interface IOpenFileDialogOutData {
  path: string;
  ext?: string;
}

@Component({
  selector: 'app-open-file-dialog',
  templateUrl: './open-file-dialog.component.html'
  //standalone: true,
})
export class OpenFileDialogComponent implements OnInit, OnDestroy {
  util = Util;
  fileTypeEnum = FileTypeEnum;
  workDir!: string;
  dirs: Array<DirectoryEntryModel> = [];
  files: Array<DirectoryEntryModel> = [];
  selectedFileIndex: number = -1;
  selectedFile: string = '';
  private path!: Path;
  errorMessage?: string;

  constructor(
    private renderer: RendererService,
    public dialogRef: MatDialogRef<OpenFileDialogComponent>
  ) {
    console.log('OpenFileDialogComponent[CTOR]');
  }

  async ngOnInit() {
    console.log('OpenFileDialogComponent[ngOnInit]');

    if (! await this.getEnvironmentData()) {
      return;
    }

    this.getDirContent(this.workDir);
  }

  ngOnDestroy(): void {
    console.log('OpenFileDialogComponent[ngOnDestroy]');
  }

  prepareLists(data?: Array<DirectoryEntryModel>) {
    if (!data) {
      return;
    }

    this.dirs = data.filter(x => x.type == FileTypeEnum.DIR || x.type == FileTypeEnum.PARENT) || [];
    this.files = data.filter(x => x.type != FileTypeEnum.DIR && x.type != FileTypeEnum.PARENT) || [];
  }

  closeDialog(isCancel: boolean) {
    let result: IOpenFileDialogOutData = { path: '' };

    if (isCancel) {
      this.dialogRef.close(result);
      return;
    }

    result.path = this.path.join(this.workDir, this.selectedFile);
    result.ext = this.path.extname(this.selectedFile);
    this.dialogRef.close(result);
  }

  selectDir(index: number) {
    this.selectedFileIndex = -1;
    this.selectedFile = '';
    let dirEntry = this.dirs[index];
    let path = (dirEntry.type == FileTypeEnum.PARENT)
      ? this.path.dirname(this.workDir)
      : this.path.join(this.workDir, dirEntry.name);
    this.getDirContent(path);
  }

  selectFile(index: number) {
    this.selectedFileIndex = index;
    this.selectedFile = this.files[index].name;
  }

  async getEnvironmentData(): Promise<boolean> {
    this.errorMessage = '';
    let response = await this.renderer.getEnvironmentData();

    if (!response.ok) {
      console.log('OpenFileDialogComponent[getEnvironmentData]:', response.error);
      this.errorMessage = response.error;
      return false;
    }

    this.path = new Path(response.data?.platform, response.data?.cwd, response.data?.env);
    this.workDir = response.data?.cwd || '.';
    return true;
  }

  async getDirContent(path: string) {
    this.errorMessage = '';
    let response = await this.renderer.getDirContent(path);

    if (!response || !response.ok) {
      console.log('OpenFileDialogComponent[getDirContent]:', response.error);
      this.errorMessage = response.error;
      return;
    }

    if (!response.data || response.data?.length == 0) {
      this.errorMessage = 'Empty directory list';
      return;
    }

    this.workDir = response.data[0].name;
    this.prepareLists(response.data);
  }

}
