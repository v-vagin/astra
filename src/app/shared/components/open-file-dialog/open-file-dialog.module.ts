import { NgModule } from '@angular/core';
import { AngularModule } from 'src/app/shared/modules/angular.module';

import { OpenFileDialogComponent } from './open-file-dialog.component';


@NgModule({
  declarations: [
    OpenFileDialogComponent
  ],
  imports: [
    AngularModule,
  ],
  exports: [
  ]
})
export class OpenFileDialogModule { }
