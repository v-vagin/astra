import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonacoLoaderService {
  onLoaded: Subject<any> = new Subject();

  constructor() {
    console.log('MonacoLoaderService[CTOR]');
  }

  createEditor(container: HTMLElement,
    options?: monaco.editor.IStandaloneEditorConstructionOptions | undefined,
    override?: monaco.editor.IEditorOverrideServices | undefined
  ): void {
    try {
      if (!container) {
        console.log('Editor container is empty');
        return;
      }

      let id = 'amd-loader';
      let msg = 'MonacoLoaderService[createEditor]: Instance of Monaco editor created. Language:';
      let el = document.getElementById(id)

      if (!el) {
        el = document.createElement("script");
        el.setAttribute('id', id);
        el.setAttribute('src', 'monaco/min/vs/loader.js');
        el.setAttribute('type', "text/javascript");
        el.setAttribute('async', 'true');
        document.body.appendChild(el);
        el.addEventListener('load', () => {
          console.log('MonacoLoaderService[createEditor]: AMD loader ready');
          window.require.config({paths:{vs:'monaco/min/vs'}});
          window.require(['vs/editor/editor.main'],()=>{
            window.editorInstance = monaco.editor.create(container, options, override);
            console.log(msg, options?.language);
            this.onLoaded.next(true);
          });
        });
        el.addEventListener('error', (error) => {
          console.log('MonacoLoaderService[createEditor]: Error on loading Monaco editor module:', error);
        });
      }
      else {
        window.editorInstance?.dispose();
        window.require.config({paths:{vs:'monaco/min/vs'}});
        window.require(['vs/editor/editor.main'],()=>{
          window.editorInstance = monaco.editor.create(container, options, override);
          console.log(msg, options?.language);
          this.onLoaded.next(true);
        });
    }
    } catch (error) {
      console.log('MonacoLoaderService[createEditor]:', error);
    }
  }

}
