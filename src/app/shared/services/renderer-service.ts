import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CommandEnum } from 'src/app/shared/enums/command-enum';
import { environment } from "src/environment";
import { PlatformTypeEnum } from '../enums/platform-type-enum';
import { EnvironmentModel } from '../models/environtment-model';
import { DirectoryEntryModel } from '../models/directory-entry-model';
import { ElectronApiRequestModel } from 'src/app/shared/models/electron-api-request-model';
import { ElectronApiResponsetModel } from '../models/electron-api-response-model';

/**
 * Renderer service is a singleton for interacting with Electron main process
 */
@Injectable({
  providedIn: 'root'
})
export class RendererService {
  electronEvent: Subject<ElectronApiRequestModel<any>> = new Subject<ElectronApiRequestModel<any>>();
  // cwd?: string = '.';
  // env?: any = {};
  // platform?: PlatformTypeEnum = PlatformTypeEnum.NONE;

  constructor() {
    console.log("Renderer[CTOR]");

    if (!environment.electron) {
      return;
    }

    console.log('node:', window.electron.version.node());
    console.log('chrome:', window.electron.version.chrome());
    console.log('electron:', window.electron.version.electron());

    this.fromMainEventHandler();
  }

  fromMainEventHandler() {
    window.electron.fromMain((e: any, response: ElectronApiRequestModel<any>) => {
      if (!response || !response.cmd) {
        return;
      }

      this.electronEvent.next(new ElectronApiRequestModel<any>(response.cmd, response.data));
    });
  }

  async getEnvironmentData(): Promise<ElectronApiResponsetModel<EnvironmentModel>> {
    let request = new ElectronApiRequestModel<void>(CommandEnum.GET_ENV);
    let response: ElectronApiResponsetModel<EnvironmentModel> = await window.electron.toMain(request)
    let result = new ElectronApiResponsetModel<EnvironmentModel>(false, '', new EnvironmentModel());

    if (!response) {
      result.error = 'Renderer[getEnvironmentData]: Empty response';
      return result;
    }

    if (!response.ok) {
      result.error = response.error;
      return result;
    }

    result.ok = true;
    result.data = response.data;
    // this.platform = response.data?.platform;
    // this.cwd = response.data?.cwd;
    // this.env = response.data?.env;
    return result;
  }

  async getWorkDir(): Promise<ElectronApiResponsetModel<string>> {
    let request = new ElectronApiRequestModel<void>(CommandEnum.GET_WORK_DIR);
    let response: ElectronApiResponsetModel<string> = await window.electron.toMain(request);
    let result = new ElectronApiResponsetModel<string>(false);

    if (!response) {
      result.error = 'Renderer[getWorkDir]: Empty response';
      return result;
    }

    if (!response.ok) {
      result.error = response.error;
      return result;
    }

    result.ok = true;
    result.data = response.data;
    return result;
  }

  async getDirContent(path?: string): Promise<ElectronApiResponsetModel<Array<DirectoryEntryModel>>> {
    let request = new ElectronApiRequestModel<string>(CommandEnum.GET_DIR_CONTENT, path);
    let response: ElectronApiResponsetModel<Array<DirectoryEntryModel>> = await window.electron.toMain(request);
    let result = new ElectronApiResponsetModel<Array<DirectoryEntryModel>>(false, '', []);

    if (!response) {
      result.error = 'Renderer[getDirContent]: Empty response';
      return result;
    }

    if (!response.ok) {
      result.error = response.error;
      return result;
    }

    result.ok = true;
    result.data = response.data;
    return result;
  }

  async readFile(path: string): Promise<ElectronApiResponsetModel<any>> {
    let request = new ElectronApiRequestModel<string>(CommandEnum.READ_FILE, path);
    let response: ElectronApiResponsetModel<any> = await window.electron.toMain(request);
    let result = new ElectronApiResponsetModel<any>(false);

    if (!response) {
      result.error = 'Renderer[readFile]: Empty response';
      return result;
    }

    if (!response.ok) {
      result.error = response.error;
      return result;
    }

    result.ok = true;
    result.data = response.data;
    return result;
  }

  quitApp() {
    let request = new ElectronApiRequestModel<string>(CommandEnum.QUIT_APP);
    window.electron.toMain(request);
  }
}
