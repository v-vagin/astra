export enum PlatformTypeEnum {
  NONE = 0,
  AIX,
  DARWIN,
  FREEBSD,
  LINUX,
  OPENBSD,
  SUNOS,
  WIN32,
  POSIX
}
