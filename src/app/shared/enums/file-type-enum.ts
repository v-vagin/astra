export enum FileTypeEnum {
  NONE = 0,
  FILE = 1,
  DIR = 2,
  BDEV = 3,
  CDEV = 4,
  LINK = 5,
  PIPE = 6,
  SOCKET = 7,
  PARENT = 8
}
