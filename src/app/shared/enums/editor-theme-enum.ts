export type EditorThemeType = 'vs' |
  'vs-dark'
;
