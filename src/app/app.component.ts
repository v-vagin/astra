import { Component, ElementRef, ViewChild, OnInit, OnDestroy, NgZone } from '@angular/core';
import { lastValueFrom } from "rxjs";

import { environment } from 'src/environment';
import { RendererService } from './shared/services/renderer-service';
import { MonacoLoaderService } from './shared/services/monaco-loader.service';
import { HelperService } from './shared/services/helper-service';

import { MatDialog } from '@angular/material/dialog';

import { IOpenFileDialogOutData, OpenFileDialogComponent } from "src/app/shared/components/open-file-dialog/open-file-dialog.component";
import { DirectoryEntryModel } from 'src/app/shared/models/directory-entry-model';
import { ElectronApiRequestModel } from 'src/app/shared/models/electron-api-request-model';
import { CommandEnum } from 'src/app/shared/enums/command-enum';
import { EditorLanguageType } from "src/app/shared/enums/editor-language-enum";
import { EditorThemeType } from "src/app/shared/enums/editor-theme-enum";

import { NewFileDialogData, NewFileDialogComponent } from './shared/components/new-file-dialog/new-file-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'astra';
  isElectronAvailable: boolean = false;
  editorTheme: EditorThemeType = 'vs';

  @ViewChild('monacoEditor', { static: true }) editorContainer!: ElementRef;
  monacoEditor!: monaco.editor.IStandaloneCodeEditor;

  /**
   *
   */
  constructor(
    private zone: NgZone,
    public renderer: RendererService,
    private monacoLoader: MonacoLoaderService,
    private helper: HelperService,
    private matDialog: MatDialog
  ) {
    console.log('AppComponent[CTOR]');
    this.isElectronAvailable = environment.electron;

    this.renderer.electronEvent.subscribe((e: ElectronApiRequestModel<any>) => {
      this.electronEventHandler(e);
    });

    this.monacoLoader.onLoaded.subscribe(() => {
      this.monacoEditor = window.editorInstance;
      this.monacoEditor.onDidChangeModelContent((e: monaco.editor.IModelContentChangedEvent) => {
      });
        // this.createEditor();
    });
  }

  ngOnInit(): void {
    console.log('AppComponent[ngOnInit]');

    // if (!this.isElectronAvailable) {
    //   setTimeout(() => {
    //   }, 1000);
    // }
  }

  ngOnDestroy(): void {
    console.log('AppComponent[ngOnDestroy]');
    this.monacoLoader.onLoaded.unsubscribe();
    this.renderer.electronEvent.unsubscribe();
  }

  /**
   *
   * @param e
   */
  electronEventHandler(e: ElectronApiRequestModel<any>) {
    console.log('AppComponent[electronEventHandler]:', e.cmd);
    switch (e.cmd) {
      case CommandEnum.MENU_NEW_FILE:
        this.newFileDialog();
        break;
      case CommandEnum.MENU_OPEN_FILE:
        this.openDialog();
        break;
      case CommandEnum.MENU_SAVE_FILE:
        break;
      case CommandEnum.MENU_SAVE_FILE_AS:
        break;
      case CommandEnum.MENU_CLOSE_FILE:
        break;
      case CommandEnum.MENU_EXIT:
        this.renderer.quitApp();
        break;
      case CommandEnum.MENU_HELP:
        break;
      case CommandEnum.MENU_ABOUT:
        break;
    }
  }

  /**
   *
   */
  newFileDialog() {
    // Using NgZone is needed to return to Angular event handler, because when clicking on menu in Electron app you are leaving Angular event handler.
    this.zone.run(async () => {
      let dialogRef = this.matDialog.open(NewFileDialogComponent, { autoFocus: false, minWidth: '300px', maxWidth: '96vw', minHeight: '200px' }).afterClosed();
      let result = await lastValueFrom<NewFileDialogData>(dialogRef);
      let name = result.name;
      let language: EditorLanguageType = result.language as EditorLanguageType;

      if (!name) {
        return;
      }

      let options: monaco.editor.IStandaloneEditorConstructionOptions = {
        value: '',
        theme: this.editorTheme,
        language: language,
        lineNumbers: 'off',
        minimap: { enabled: false },
        wordWrap: 'on',
        wrappingIndent: 'indent',
        automaticLayout: true
      };
      this.monacoLoader.createEditor(this.editorContainer.nativeElement, options);
    });
  }

  openDialog() {
    // Using NgZone is needed to return to Angular event handler, because when clicking on menu in Electron app you are leaving Angular event handler.
    this.zone.run(async () => {
      let dialogRef = this.matDialog.open(OpenFileDialogComponent, { autoFocus: false, minWidth: '300px', maxWidth: '96vw', minHeight: '200px' }).afterClosed();
      let result = await lastValueFrom<IOpenFileDialogOutData>(dialogRef);

      if (!result || !result.path) {
        return;
      }

      let response = await this.renderer.readFile(result.path);

      if (!response || !response.ok) {
        console.log('AppComponent[openDialog]:', response.error);
        return;
      }

      let language = this.extToLanguage(result.ext || 'qweqweqwe');

      if (language == 'bin') {
        console.log('AppComponent[openDialog]: Not a text file');
        return;
      }

      let options: monaco.editor.IStandaloneEditorConstructionOptions = {
        value: response.data,
        theme: this.editorTheme,
        language: language,
        lineNumbers: 'off',
        minimap: { enabled: false },
        wordWrap: 'on',
        wrappingIndent: 'indent',
        automaticLayout: true
      };
      this.monacoLoader.createEditor(this.editorContainer.nativeElement, options);
    });
  }

  private extToLanguage(ext: string): string {
    if (!ext) {
      return '';
    }

    switch (ext) {
      case '.cpp': return 'cpp';
      case '.csharp': return 'csharp';
      case '.css': return 'css';
      case '.html': return 'html';
      case '.java': return 'java';
      case '.js': return 'javascript';
      case '.json': return 'json';
      case '.less': return 'less';
      case '.lua': return 'lua';
      case '.markdown':
      case '.mdown':
      case '.mkdn':
      case '.mkd':
      case '.mdwn':
      case '.mdtxt':
      case '.mdtext':
      case '.md': return 'markdown';
      case '.sql': return 'sql';
      case '.c': return 'objective-c';
      case '.pl': return 'perl';
      case '.php': return 'php';
      case '.py': return 'python';
      case '.razor': return 'razor';
      case '.scss': return 'scss';
      case '.sh': return 'shell';
      case '.ts': return 'typescript';
      case '.vb': return 'vb';
      case '.xml': return 'xml';
      case '.yaml': return 'yml';
      case '.exe':
      case '.bin':
      case '.dll':
      case '.ico':
      case '.sys':
      case '.com':
      case '': return 'bin';
      default: return '';
    }
  }
}
