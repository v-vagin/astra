export interface IElectron {
  fromMain: (callback: any) => {},
  toMain: (data: any) => Promise<any>,
  version: {
    node: () => {},
    chrome: () => {},
    electron: () => {}
  }
}

declare global {
  interface Window {
    electron: IElectron, // integration with Electron
    require: AMDLoader.IRequireFunc, // integration with AMD Loader
    monaco: typeof monaco.editor, // integration with Monaco editor
    MonacoEnvironment?: monaco.Environment, // integration with Monaco editor
    // editorLanguage: string, // Save
    editorInstance: monaco.editor.IStandaloneCodeEditor // reference to new created intance of Monaco editor
  }
}
