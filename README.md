# Astra

Текстовый редактор на базе [monaco editor](https://microsoft.github.io/monaco-editor/) с интерфейсом на [Angular](https://angular.io) + [Angular Material](https://material.angular.io/) под платформу [electron](https://www.electronjs.org/).

Данное приложение является тестовым и создано только для изучения интеграции выше указанных фреймворков и программных пакетов.

## Сборка

`git clone https://gitlab.com/v-vagin/astra.git`

`cd astra`

`npm install`

`npm run build`

## Запуск в среде electron

`npm run electron`

## Сборка исполняемых неустановочных файлов

Для получения исполняемых файлов под одну из ОС (Windows, Linux или MacOS), сборку необходимо осуществлять на компьютере с соответствующей ОС комадной:

`npm run build-astra`

в папке astra-electron\win-unpacked\ - будет находится исполняемый неустановочный файл для Windows.

в папке astra-electron\mac\ - будет находится исполняемый неустановочный файл для MacOS.

## Сборка установочных файлов (дистрибутивов)

Для получения установочных файлов под одну из ОС (Windows, Linux или MacOS), сборку необходимо осуществлять на соответствующей ОС комадной:

`npm run dist-astra`

в папке astra-electron\ - будет находится установочный файл

## Причеание

На данный момент в меню приложения реализованы три пункта: `Новый файл`, `Открыть файл` и `Выход`
