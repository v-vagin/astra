const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
import * as webpack from 'webpack';

export default (config: webpack.Configuration) => {
  config?.plugins?.push(new MonacoWebpackPlugin({
    languages: ['javascript', 'css', 'html', 'typescript', 'json'],
  }));
  // Remove the existing css loader rule
  const cssRuleIdx = config?.module?.rules?.findIndex((rule: any) =>
    rule.test?.toString().includes(':css')
  );
  if (cssRuleIdx !== -1) {
    config?.module?.rules?.splice(cssRuleIdx!, 1);
  }
  config?.module?.rules?.push(
    {
      test: /\.ttf$/,
      use: ['file-loader']
    },
    {
      test: /\.css$/,
      use: ['style-loader', 'css-loader'],
    }
  );
  return config;
};

/*

new MonacoWebpackPlugin({
  languages: ['javascript', 'css', 'html', 'typescript', 'json'], // EditorLanguage[];
  // EditorLanguage = 'abap' | 'apex' | 'azcli' | 'bat' | 'bicep' | 'cameligo' | 'clojure' |
  // 'coffee' | 'cpp' | 'csharp' | 'csp' | 'css' | 'cypher' | 'dart' | 'dockerfile' | 'ecl' |
  // 'elixir' | 'flow9' | 'freemarker2' | 'fsharp' | 'go' | 'graphql' | 'handlebars' | 'hcl' |
  // 'html' | 'ini' | 'java' | 'javascript' | 'json' | 'julia' | 'kotlin' | 'less' | 'lexon' |
  // 'liquid' | 'lua' | 'm3' | 'markdown' | 'mips' | 'msdax' | 'mysql' | 'objective-c' | 'pascal' |
  // 'pascaligo' | 'perl' | 'pgsql' | 'php' | 'pla' | 'postiats' | 'powerquery' | 'powershell' |
  // 'protobuf' | 'pug' | 'python' | 'qsharp' | 'r' | 'razor' | 'redis' | 'redshift' | 'restructuredtext' |
  // 'ruby' | 'rust' | 'sb' | 'scala' | 'scheme' | 'scss' | 'shell' | 'solidity' | 'sophia' | 'sparql' |
  // 'sql' | 'st' | 'swift' | 'systemverilog' | 'tcl' | 'twig' | 'typescript' | 'vb' | 'wgsl' | 'xml' | 'yaml';

  features: ['!gotoSymbol'], // (EditorFeature | NegatedEditorFeature)[]
  // EditorFeature = 'anchorSelect' | 'bracketMatching' | 'browser' | 'caretOperations' | 'clipboard' | 'codeAction' |
  //   'codelens' | 'colorPicker' | 'comment' | 'contextmenu' | 'cursorUndo' | 'dnd' | 'documentSymbols' | 'dropOrPasteInto' |
  //   'find' | 'folding' | 'fontZoom' | 'format' | 'gotoError' | 'gotoLine' | 'gotoSymbol' | 'hover' | 'iPadShowKeyboard' |
  //   'inPlaceReplace' | 'indentation' | 'inlayHints' | 'inlineCompletions' | 'inlineProgress' | 'inspectTokens' | 'lineSelection' |
  //   'linesOperations' | 'linkedEditing' | 'links' | 'longLinesHelper' | 'multicursor' | 'parameterHints' | 'quickCommand' | 'quickHelp' |
  //   'quickOutline' | 'readOnlyMessage' | 'referenceSearch' | 'rename' | 'semanticTokens' | 'smartSelect' | 'snippet' | 'stickyScroll' |
  //   'suggest' | 'toggleHighContrast' | 'toggleTabFocusMode' | 'tokenization' | 'unicodeHighlighter' | 'unusualLineTerminators' | 'wordHighlighter' |
  //   'wordOperations' | 'wordPartOperations';
  // NegatedEditorFeature = '!anchorSelect' | '!bracketMatching' | '!browser' | '!caretOperations' | '!clipboard' | '!codeAction' | '!codelens' |
  //    '!colorPicker' | '!comment' | '!contextmenu' | '!cursorUndo' | '!dnd' | '!documentSymbols' | '!dropOrPasteInto' | '!find' | '!folding' |
  //    '!fontZoom' | '!format' | '!gotoError' | '!gotoLine' | '!gotoSymbol' | '!hover' | '!iPadShowKeyboard' | '!inPlaceReplace' | '!indentation' |
  //    '!inlayHints' | '!inlineCompletions' | '!inlineProgress' | '!inspectTokens' | '!lineSelection' | '!linesOperations' | '!linkedEditing' |
  //    '!links' | '!longLinesHelper' | '!multicursor' | '!parameterHints' | '!quickCommand' | '!quickHelp' | '!quickOutline' | '!readOnlyMessage' |
  //    '!referenceSearch' | '!rename' | '!semanticTokens' | '!smartSelect' | '!snippet' | '!stickyScroll' | '!suggest' | '!toggleHighContrast' |
  //    '!toggleTabFocusMode' | '!tokenization' | '!unicodeHighlighter' | '!unusualLineTerminators' | '!wordHighlighter' | '!wordOperations' | '!wordPartOperations';
});

*/